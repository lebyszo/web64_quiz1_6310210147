import './AboutUs.css';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function AboutUs(props) {

    return(
        <Box sx={{width: "50%"}}>
            <Paper elevation={5}>
                <h2>จัดทำโดย <h2 class="Name">{props.name}</h2></h2>
                <h3>รหัสนักศึกษา <h3 class="UID">{props.uid}</h3></h3>
                <h3>ติดต่อ <h3 class="Add">{props.address}</h3></h3>
            </Paper>
        </Box>
    );
}

export default AboutUs;