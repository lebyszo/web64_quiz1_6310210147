import logo from './logo.svg';
import './App.css';
import Header from './Components/Header';
import OddEvenPage from './Pages/OddEvenPage';
import AboutUsPage from './Pages/AboutUsPage';
import { Routes, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={
          <OddEvenPage />
        } />

        <Route path="/about" element={ 
          <AboutUsPage />
        } />

      </Routes>
    </div>
  );
}

export default App;
