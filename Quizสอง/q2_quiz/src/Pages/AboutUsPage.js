import AboutUs from "../Components/AboutUs";

function AboutUsPage() {

    return(
        <div>
            <div align="center">
                <h2>ผู้จัดทำเว็บนี้</h2>
                <AboutUs name="ถิรพงศ์ ไทยทอง"
                         uid="6310210147"
                         address="6310210147@psu.ac.th" />
                         
            </div>
        </div>
    );
}

export default AboutUsPage;