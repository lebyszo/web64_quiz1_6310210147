import { useState } from "react";
import Button from '@mui/material/Button';
import * as React from 'react';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from "@mui/material";
import OddEvenResult from "../Components/OddEvenResult";

function OddEvenPage() {

    const [ number , setNum ] = useState("");
    const [ result , setResult] = useState("");
 
    function FindOddEven() {
        let N = parseInt(number);
        setNum(N);
        if(N == 0)
        {
            setResult("ศูนย์");
        }
        else if(N%2 == 0)
        {
            setResult("เลขคู่");
        }
        else if(N%2 == 1)
        {
            setResult("เลขคี่")
        }
        else
        {
            setResult("ใส่ตัวเลขก่อน !!!")
        }
    }

    return(
        <Container maxWidth="lg">
            <Grid container spacing={2} sx={{ marginTop : "10px"}}>

                <Grid item xs={12}>
                    <Typography variant="h5">ยินดีต้อนรับสู่เว็บหาเลขคู่/คี่</Typography>
                </Grid>
                <Grid item xs={8}>
                    <Box sx={{ textAlign : "left"}}>
                        ตัวเลข <input type="text" 
                                    value={number}
                                    onChange={(e) => {setNum(e.target.value);}} /> <br /><br />

                        <Button variant="contained" onClick={() => FindOddEven()}>Find</Button>
                </Box>
                </Grid>
                <Grid item xs={4}>
                { result != 0 && 
                    <div> 
                        <hr />

                        ผลลัพธ์

                        <OddEvenResult 
                            result = { result }
                        />
                    </div>
                }
                </Grid>
            </Grid>
        </Container>
    );
}

export default OddEvenPage;